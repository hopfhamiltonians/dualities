\documentclass[aps,preprint]{revtex4-1}
\usepackage{graphicx,psfrag,color}% Include figure files
\usepackage{mathbbol,amsmath,amsfonts,amssymb,bm,dsfont}
\usepackage{wasysym}
\usepackage{amsthm}




\def\dual{\ \stackrel{\Phi_\d}{\longrightarrow}\ }
\def\idual{\ \stackrel{\Phi^{-1}_\d}{\longrightarrow}\ }
\def\qdual{\ \stackrel{\Phi_\d (?)}{\longrightarrow}\ }
\def\l{{\bm{l}}}
\def\d{{{\sf d}}}
\def\r{{\bm{r}}}
\def\x{{\bm{x}}}
\def\i{{\bm{e_1}}}
\def\j{{\bm{e_2}}}
\def\k{{\bm{e_3}}}
\def\Z{{\mathbb{Z}}}
\def\one{{\mathbb{1}}}
\def\tr{{{\sf Tr}\ }}
\def\fg{\mathfrak{g}}
\def\pf{\mathcal{Z}}




\begin{document}

\title{Hopf Hamiltonians}

%\author{E Cobanera$^1$, E Knill and G Ortiz$^1$}

%\address{$^1$ Department of Physics, Indiana University, Bloomington,
%IN 47405, USA}
%\address{$^2$ National Institute of Standards and Technology, Boulder, CO 80305, USA}
%\ead{ecobaner@indiana.edu}

\begin{abstract}
Hopf Hamiltonians are introduced and studied using dualities. 
\end{abstract}

\maketitle

\tableofcontents

\section{Introduction}
Spin models comprise a rich playground where general ideas of statistical
and quantum mechanics can be put to the test. \(G\)-models, where \(G\) denotes
a group, form an important subfamily of spin models that is tractable in many
respects due to the simplifications inherited from the group structure in
the configuration space \(G\) of each spin. In mathematics, 
Hopf algebras generalize simultaneously the notion of group and the notion of Lie algebra,
while keeping many of those objects appealing features. Hence we propose to study the 
generalization of \(G\)-models to Hopf algebras, by constructing Hopf Hamiltonians. 
For motivation and guidance, we will first describe classical and quantum \(G\)-models,
and recast them in terms of the group Hopf algebra \(H(G)\). Once this is done, 
it follows immediately that the we can replace \(H(G)\) by an arbitrary Hopf algebra
\cite{kassel}
while retaining the basic features of the Hamiltonian. The call the resulting Hamiltonian
a Hopf Hamiltonian. Next we 
study the bond algebra %\cite{con,conII} 
of Hopf Hamiltonians to obtain
a dual Hopf Hamiltonian, and the corresponding dual variables (collective excitations).

\section{Motivation: Group Hamiltonians}

\subsection{From Classical to Quantum \(G\)-models}
A classical spin is a localized system that can be in a number of (internal) 
states described by the points of a configuration space. \(G\)-models are spin
models for which the configuration space of the spins is a group \(G\), and the
interactions can be partially described in terms of the group structure. The best 
studied example is the exactly-solvable classical Ising model, that can be seen
as a \(\mathds{Z}_2\)-model.
For concreteness, consider a square lattice with spins on its sites. The state of any
spin is described by a point in a group \(G\) 
(that we imagine discrete and even finite for simplicity). Then we may be
able to write 
the interaction energy between sites two nearest neighbours \(\r, \r'\) 
in the lattice as
\begin{equation}
\epsilon(g_\r,g_{\r'})=v(g_\r g_{\r'}^{-1})
\end{equation}
where the real-valued function \(v:G\rightarrow \mathbb{R}\) should be 
bounded below. The interaction energy is symmetric, \(
\epsilon(g_\r,g_{\r'})=\epsilon(g_{\r'},g_{\r})\) if
\begin{equation}
v(g^{-1})=v(g),
\end{equation}
as usually happens for physically motivated models.
Notice that we use the group structure to define out of the states 
\(g_\r\) and \(g_{\r'}\) the {\it relative state} \(g_\r g_{\r'}^{-1}\).
The interaction energy is a function of the relative state only.
The partition function of this spin system reads
\begin{equation}
{\mathcal Z}_{G}=\sum_{\{g\}} 
\exp\Big[-\beta\sum_\r\sum_{\nu=1,2} v(g_{\r+\bm{e_\nu}}g_\r^{-1})\Big],
\end{equation}
discarding all but nearest neighbor interactions. This completes
the description of an example of a classical \(G\)-model.
In the following, we absorb the temperature factor by redefining 
\(-\beta v\rightarrow v\). 

Next we would like to define quantum \(G\)-models so 
that they are, in some sense to be specified, 
the natural counterparts of the classical \(G\)-models just described.
In classical statistical mechanics, transfer matrices affords
a popular bridge between classical and quantum, so we turn to the problem
of rewriting \({\mathcal Z}_{G}\) in terms of transfer matrices.


First we introduce the Hilbert space
\begin{equation}
\mathcal{H}=\bigotimes_{i\in\mathbb{Z}} H_i
\end{equation}
to describe the state of the spins on any row of the system. 
For a \(G\) model, \(H_i=V(G)\), the vector space
freely generated by the elements of \(G\), that we 
will denote in this contex as \(|g\rangle, g\in G\). Then we can
define a Hermitian inner product on \(V(G)\) by \(\langle g|h\rangle=\delta(g,h)\).
The vectors \(|\{g\}\rangle\equiv\bigotimes_i |g_i\rangle\) form a canonical
basis for the many-body Hilbert space \(\mathcal{H}\). 

Second, we need to define a suitable family of row operators. We introduce
site operators 
\begin{equation}
L_i(h)|\{g\}\rangle\equiv|\cdots g_{i-1}\ hg_i\ g_{i+1}\cdots\rangle,
\end{equation}
that act on site \(i\) of the row. Then \([L_i(g),L_j(h)]=0\) if \(i\neq j\), and 
\begin{equation}
L_i(h)L_i(g)=L_i(hg), \qquad L_i(h)^\dagger=L_i(h^{-1}).
\end{equation}
The self-adjoint link operators
\begin{equation}
\Delta_{(i,1)}(h)|\{g\}\rangle\equiv|\{g\}\rangle \delta(h,g_{i+1}g_i^{-1}),
\end{equation}
act on two nearest-neighbor sites of the row,
commute on different links, and  
\begin{equation}
\Delta_{(i,1)}(h)\Delta_{(i,1)}(h')=\delta(h,h')\Delta_{(i,1)}(h')
\end{equation}
(the relations between \(L\)s and \(\Delta\)s are described by Eqs. (\ref{rels}) and 
(\ref{rels2})). The Kronecker \(\delta\) on \(G\) is defined in the obvious fashion,
\begin{equation}
\delta(g,g')=\left\{
\begin{array}{lcr}
1& \mbox{if}& g=g'\\
0& \mbox{if}& g\neq g'
\end{array}
\right. .
\end{equation}

Now one can check that \(\pf\) 
(with suitable boundary conditions) can be written
as \(\pf=\tr [T_1T_0]^N\), with
\begin{equation}
T_0=\exp\Big[\sum_i \sum_{h\in G}v(h)\Delta_{(i,1)}(h)\Big],\qquad 
T_1=\prod_i\ \sum_{h\in G} e^{v(h)}L_i(h),
\end{equation}
provided it is  understood that the trace should be computed in the canonical
basis \(|\cdots g_i\cdots\rangle\).
Since the set of \(L_i(h),\ h\in G\) is closed under multiplication, 
we can write
\begin{equation}
\sum_{h\in G} e^{v(h)}L_i(h)=e^{\sum_{h\in G} \lambda(h) L_i(h)},  
\end{equation}
for suitably chosen couplings \(\lambda(h)\).
This calculation justifies using the non-irreducible left regular representation
as the basic operator language to describe \(G\)-models. 
From now on we focus on the associated %\cite{fradkin_susskind} 
\(d=1\) Hamiltonian
\begin{equation}\label{hg}
H_G=\sum_i\sum_{h\in G}\left[\lambda(h)L_i(h)+v(h)\Delta_{(i,1)}(h)\right]. 
\end{equation} 

\subsection{Duality transformation}
The \(G\) model \(H_G\) admits a duality transformation regardless
of the character (Abelian or non-Abelian) of \(G\).
If we consider the bond algebra 
of Eq. \ref{gba}, then on top of the relations already discussed we need two 
(up to translations) other relations,
\begin{eqnarray}\label{rels}
L_i(h)\Delta_{(i,1)}(g)=\Delta_{(i,1)}(gh^{-1})L_i(h),\label{rels}\\
L_{i+1}(h)\Delta_{(i,1)}(g)=\Delta_{(i,1)}(hg)L_{i+1}(h).\label{rels2}
\end{eqnarray}
It is not difficult to find a dual Hamiltonian. Let us introduce the operators
(right regular representation)
\begin{eqnarray}
R_i(h)|\{g\}\rangle\equiv|\cdots g_{i-1}\ g_ih^{-1}\ g_{i+1}\cdots\rangle,\\
\delta_i(h)|\{g\}\rangle\equiv|\{g\}\rangle\delta(h,g_i).
\end{eqnarray}
Then the Hamiltonian
\begin{equation}
H_{G}^D=\sum_i\sum_{h\in G} \big[\lambda(h)L_i(h)R_{i+1}(h)+v(h)\delta_i(h) \big]
\end{equation}
is dual to \(H_G\). The duality isomorphism is defined on the bond generators
as
\begin{equation}\label{duality}
L_i(h)\dual L_i(h)R_{i+1}(h),\ \ \ \ \ \ \ \ \ \
\Delta_{(i,1)}(h)\dual\delta_{i+1}(h).
\end{equation}

To confirm that this mappings preserve all relations, we 
further notice that
\begin{eqnarray}
(L_i(h)R_{i+1}(h)) \delta_{i+1}(g)=\delta_{i+1}(gh^{-1}) (L_i(h)R_{i+1}(h)),\\ 
(L_{i+1}(h)R_{i+2}(h))\delta_{i+1}(g)=\delta_{i+1}(hg)(L_{i+1}(h)R_{i+2}(h)),
\end{eqnarray}
to be compared to Eq. \ref{rels}. Also,
\begin{eqnarray}
\delta_{i}(h)\delta_i(h')=\delta(h,h')\delta_i(h'),\\
(L_i(h)R_{i+1}(h))(L_i(h')R_{i+1}(h'))=L_i(hh')R_{i+1}(hh'),
\end{eqnarray}
and 
\begin{equation}
[L_i(h)R_{i+1}(h),L_j(h')R_{j+1}(h')]=0
\end{equation}
always.


\subsection{ \(G\)-fermions}

G-fermions are a class of lattice fields associated to arbitrary, discrete and finite groups G. The exchange statistics of G-fermions are
determined by the left regular representation of \(G\) and a choice of a one-dimensional representation of G. 
If \(G=\Z_2\), G-fermions coincide with Majoranas, and if \(G=\Z_p\), G-fermions coincide with parafermions. 
Using Kitaevs ``pairing'' strategy, we construct models of G-fermions with a rich structure of zero-energy edge modes. 
We would like to investigate Fock representations of G-fermions.

Let G be a finite, discrete group. There is a natural Hilbert space associated to G, spanned by vectors
\begin{eqnarray}
|h\rangle, \quad h\in G,
\end{eqnarray}
declared to be orthonormal,
\begin{eqnarray}
\langle h|h'\rangle =\delta_{g,g'}.
\end{eqnarray}
The Weyl matrices are a special case of  the following construction. First, let us define 
a formal ``position operator'' as 
\begin{eqnarray}
\hat{h}|h\rangle=h|h\rangle.
\end{eqnarray}
The meaning is the following. For any function \(f:G\rightarrow\mathds{C}\), there is a diagonal matrix 
\begin{eqnarray}
f(\hat{h})|h\rangle=f(h)|h\rangle. 
\end{eqnarray}
For example, let \(G=\Z_p={0,\dots,p-1}\), and let \(f(i)=e^{\sqrt{-1}2\pi i/p}\). Then 
\begin{eqnarray}
f(\hat{h})=U,\quad \mbox{since}\quad e^{\sqrt{-1}2\pi \hat{h}/p}|i\rangle=e^{\sqrt{-1}2\pi i/p}|i\rangle\quad (i\in\Z_p).
\end{eqnarray}
``Translation'' operators are 
\begin{eqnarray}
L^g|h\rangle=|gh\rangle, \quad (L^g)^\dagger=L^{g^{-1}}.
\end{eqnarray}
For example, for \(G=\Z_p={0,\dots,p-1}\),
\begin{eqnarray}
L^1=V^\dagger. 
\end{eqnarray}

For a general group, the role of the V matrix is played by all of the \(L^g\). There is no natural way to choose one over another (though a
small generating set is always possible). A choice of U matrix is equivalent to choosing a representation of \(\pi\) of \(G\): the generalized
\(U\) matrix is \(\pi(\hat{h})\).  Then we have
\begin{eqnarray}
L^g\pi(\hat{h})=\sum_\gamma\pi_{\alpha\, \gamma}(g^{-1})\pi_{\gamma\, \beta}(\hat{h})L^g.
\end{eqnarray}
Notice that \(\pi(\hat{h})\) is an operator-valued matrix, while \(\pi(g^{-1})\) is just a numerical matrix. 
More compactly,
\begin{eqnarray}
L^g\pi(\hat{h})=\pi(g)^{-1}\pi(\hat{h})L^g.
\end{eqnarray}
Let \(s:G\rightarrow U(1)\) be a one-dimensional representation. Then the more usual relation 
\begin{eqnarray}
L^gs(\hat{h})=s(g)^* s(\hat{h})L^g
\end{eqnarray}
without summation is obtained.

Suppose for simplicity that the system of interest is one-dimensional. 
The Hilbert space per site \(i\) is that associated to 
a group \(G\) as above. The local translations \(L^g_i\) and position 
operators \(\hat{h}_i\) commute on different sites. 
For every one dimensional representation \(s:G\rightarrow U(1)\), we define the G-fermions
\begin{eqnarray}
\Gamma^g_i=L^g_is(\hat{h}_{i-1})\dots s(\hat{h}_1),\quad \Delta^g_i=\Gamma^g_is(\hat{h}_i).
\end{eqnarray}
The inverse transformation is
\begin{eqnarray}
s(\hat{h}_i)=(\Gamma^g_i)^\dagger \Delta_i^g,\quad L^g_i=\Gamma^g_i\prod_{l=1}^{i-1}(\Gamma^g_i)^\dagger\ \Delta_i^g.
\end{eqnarray}

They have non-trivial exchange statistics,
\begin{eqnarray}
\Gamma^g_i\Gamma^{g'}_j&=&s(g)^* \Gamma^{g'}_j \Gamma_i\quad (i<j)\\
\Delta^g_i\Delta^{g'}_j&=&s(g)^*\Delta^{g'}_j\Delta^g_i\quad (i<j)\\
\Gamma^g_i\Delta^{g'}_j&=&s(g)^*\Delta^{g'}_j\Gamma^g_i\quad (i<j)\\
\Gamma^g_i\Delta^{g'}_i&=&s(g)^*L_i^{[g,g']}\Delta^{g'}_i\Gamma^g_i.
\end{eqnarray}
The group commutator is 
\begin{eqnarray}
[g,g']=gg'g^{-1}(g')^{-1}.
\end{eqnarray}
Moreover,
\begin{eqnarray}
(\Gamma^g_i)^\dagger=\Gamma_i^{g^{-1}},\quad (\Delta^g_i)^\dagger=\Delta_i^{g^{-1}}.
\end{eqnarray}

Conspicous examples are Majoranas \(G=\Z_2\) and parafermions \(G=\Z_p\).

Choose \(g\) fixed but arbitrary. The subgroup \(S_g\) of \(G\) of elements that commute with \(g\) is
called the stabilizer of \(g\), and it need not be Abelian. Define the Hamiltonian
\begin{eqnarray}
H^g=\sum_{i=1}^{L-1}[\alpha\Gamma^{g^{-1}}_i \Delta^g_{i}
+\Delta^g_i\Gamma_{i+1}^{g^{-1}}+h.c.]
\end{eqnarray}
The for \(\alpha=0\),
\begin{eqnarray}
[H,\Gamma^s_1]=0=[H,\Delta^s_L], \quad \forall s\in S_g.
\end{eqnarray}
What is the topological degeneracy of the system? How does 
it reflect  the choice of phase \(s\) and/or the nature of the 
group \(S_g\)?





\section{Hopf Hamiltonians}

\subsection{From G-models to Hopf models}
The structure, that is, the bond operators, introduced to define the 
\(G\)-model Hamiltonian \(H_G\) are understood most naturally in terms 
of \(H(G)\), the Hopf algebra of \(G\). Let us recall this Hopf algebra briefly,
taking the complex numbers \(\mathds{C}\) for the ground field. Then
we can define a multiplication map \(m\), a unit map \(e\), 
a comultiplication map \(\Delta\), and a counit \(\epsilon\)
as 
\begin{eqnarray}
e(\alpha)=\alpha |e\rangle,\quad \forall \alpha \in \mathds{C},
\qquad m(|g\rangle|h\rangle)=|gh\rangle;\\
\epsilon(|g\rangle)=1,\quad \forall g\in G, 
\qquad \Delta(|g\rangle)=|g\rangle|g\rangle.
\end{eqnarray}
The antipode \(S\) is 
\begin{equation}
S|g\rangle=|g^{-1}\rangle.
\end{equation}
The point to notice is that we can use these operators, and essentially these
operators alone, to rewrite the \(G\)-model Hamiltonian \(H_G\).


Let \(|p\rangle=\sum_h \lambda(h)|h\rangle\). Then we can defined
a site operator on \(H(G)\)
\begin{equation}\label{kinetic}
K(p)\equiv=m(|p\rangle, \cdot)
\end{equation}, meaning 
\begin{equation}
K|g\rangle=m(|p\rangle|g\rangle)=\sum_h \lambda(h) m(|h\rangle|g\rangle)=
\sum_h\lambda(h)L_h|g\rangle.
\end{equation}
Similarly, we can define a link operator on \(H(G)\otimes H(G)\) as
\begin{equation}\label{potential}
V=(\mathds{1}\otimes(v\circ m\circ (\mathds{1}\otimes S))\otimes\mathds{1})\circ
(\delta\otimes \delta)
\end{equation}
(\(\circ\) denotes composition of maps, and \(\mathds{1}\) is the identity
operator), meaning
\begin{eqnarray}
V|g\rangle|g'\rangle= \\
(\mathds{1}\otimes(v\circ m\circ (\mathds{1}\otimes S))\otimes\mathds{1})
|g\rangle |g\rangle|g'\rangle |g'\rangle=\\
v(m(|g\rangle S|{g'}\rangle))|g\rangle|g'\rangle= v(g{g'}^{-1})|g\rangle|g'\rangle.
\end{eqnarray}
Notice that \(V\) is diagonal in the canonical basis.
It follows that
\begin{equation}\label{hghopf}
H_{G}=\sum_i (K(p)_i+V_{(i,1)}).
\end{equation}


What did we gain by rewriting the \(G\)-model Hamiltonian \(H_{G}\) as in 
Eq. (\ref{hghopf})? 
{\it The key point to notice is that the operator \(K\) of Eq. (\ref{kinetic}) and
and \(V\) of Eq. (\ref{potential}) are defined
for an arbitrary Hopf algebra}, and not just for this group setting. So
let \((H, m,e,\Delta,\epsilon,S)\) be a finite-dimensional (for simplicity)
but otherwise arbitrary Hopf algebra. We would like to define the 
Hamiltonian for a ``spin chain" where the state of the spin at site \(i\) is described by 
vectors in \(H\) (to keep the notation simple, we will denote states in \(H\) as
\(x,y,\cdots\ \in A\) rather than \(|x\rangle,|y\rangle,\cdots\)). 
So we take the state space to be \(\mathcal{H}=\bigotimes_i H\),
and assume that the Hamiltonian has the form
\begin{equation}\label{ha}
H_H=\sum_i[K_i+V_{(i,1)}]
\end{equation}
where \(K\) is given by in Eq. (\ref{kinetic}), for some fixed but arbitrary at
present \(p\in H\), and \(V\) is given by Eq. (\ref{potential}), with \(v\in H^*\)
some linear functional on \(H\). We can represent \(K\) graphically as in Fig. \ref{kin}. 
\begin{figure}[h]
\centering
\includegraphics[angle=0, width=.1\columnwidth]{kin}
\caption{Graphical representation of the one-body kinetic energy. As before, 
The lines in red represent multiplication, but the left entry is fixed.}
\label{kin}
\end{figure}

Unlike the case of the previous section, we do not know any more what is the 
diagonal basis for the operators \(V_{(i,1)}\). In general,
\begin{eqnarray}
V(x\otimes x')= \sum_{(x),(x')} x^{(1)}\otimes {x'}^{(2)} v(x^{(2)}S{x'}^{(1)}).
\end{eqnarray}
In the last equation we used Sweedler's notation to write \(\Delta(x)=\sum_{(x)}
x^{(1)}\otimes x^{(2)}\), and wrote simply \(xy\) for \(m(x\otimes y)\). See Fig.
\ref{v} for a graphical representation of \(V\).
\begin{figure}[h]
\centering
\includegraphics[angle=0, width=.3\columnwidth]{2bint}
\caption{Graphical representation of the two-body interaction \(V\).
The lines in blue represent the action of the comultiplication, the lines
in red that of the multiplication, the black cross stands for the action of the antipode, and
the green cirle for the action of the functional \(v\). The composition of mappings is done from 
bottom to top}
\label{v}
\end{figure}



\subsection{Duality transformation}

The \(G\)-models described above as a particular case of Hopf models
have a dual representation and non-local dual variables. 
In this section we show that this property generalizes to
any Hopf model. The dual Hamiltonian reads
\begin{equation}
H_A^D=\sum_i[K_i^D+V_{(i,1)}^D],
\end{equation}
where we have to specify \(K^D\) and \(V^D\).
\(V^D\) is defined as
\begin{equation}
V^D=(\mathds{1}\otimes v)\Delta,\ \ \ \ V^D(x)=\sum_{(x)}x^{(1)}v(x^{(2)}),
\end{equation}
\begin{figure}[h]
\centering
\includegraphics[angle=0, width=.1\columnwidth]{vD}
\caption{Graphical representation of \(V^D\).}
\label{vD}
\end{figure}
and \(K^D\) as 
\begin{equation}
K^D=(\mu\otimes\mu)(\mathds{1}\otimes(S\otimes\mathds{1})\Delta(p)\otimes \mathds{1}),\ \ \ \ 
K^D(x\otimes x')=\sum_{(p)} (xSp^{(1)})\otimes(p^{(2)}x').
\end{equation}

\begin{figure}[h]
\centering
\includegraphics[angle=0, width=.3\columnwidth]{kD}
\caption{Graphical representation of \(K^D\).}
\label{kD}
\end{figure}

To prove that \(H_A\) and \(H_A^D\) are (algebraically) dual, we show that the 
algebras generated by the \(K_i, V_{(i,1)}\) and \(K_i^D, V_{(i,1)}^D\) are isomorphic


\subsection{Disorder variables}

\section{Statistical Mechanics of Hopf Hamiltonians}

\subsection{Gapped states: Frustration free Hopf Hamiltonians and more
general MPS Ansatze}

\subsection{Critical states: the CFT state approach}


\begin{thebibliography}{}

\bibitem{kassel}
C. Kassel, {\it Quantum Groups} (Springer, 1995).

\bibitem{quantization}
M. de Gosson, {\it Born-Jordan Quantization} (Springer, 2016).

\end{thebibliography}




\end{document}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\appendix



\section{Abelian dualities for \(G\)-models with \(G\) non-Abelian}
Without further constraints on the problem, 
it is natural to take for bond generators of the
bond algebra \(\mathcal{A}_{H_G}\) the operators 
\begin{equation}\label{gba}
L_i(h),\qquad  \Delta_{(i,1)}(h).
\end{equation} 
However, if \(\lambda(h)\) and \(v(h)\) are class functions,
so that \(\lambda(ghg^{-1})=\lambda(h)\) and similarly for \(v\), there
is a more convenient choice of bonds.
Since \(G\) is the disjoint union of its conjugacy classes
\(G=\cup_{\mathcal{C}} \mathcal{C}\) and the couplings are constant on
each class, we can write
\begin{equation}
H_{G}=\sum_i\sum_{\mathcal{C}}\left
[\lambda_\mathcal{C} L_i(\mathcal{C})+ v_{\mathcal{C}}
\Delta_{(i,1)}(\mathcal{C})\right],
\end{equation}
with
\begin{equation}
L_i(\mathcal{C})=\sum_{h\in\mathcal{C}}L_i(h), \ \ \ \ \ \ \ \ \ 
\Delta_{(i,1)}(\mathcal{C})=\sum_{h\in\mathcal{C}}\Delta_{(i,1)}(h)\ .
\end{equation}
{\it But the \(L_i(\mathcal{C})\) (that are the relevant bonds in this case) 
generate a commutative algebra}, since
\begin{eqnarray}
L_i(\mathcal{C})L_i(\mathcal{C}')&=&\sum_{h\in\mathcal{C}, h'\in\mathcal{C}'}
L_i(h)L_i(h')=\\
\sum_{h'\in\mathcal{C}'}L_i(h')\sum_{h\in\mathcal{C}}L_i({h'}^{-1}hh')&=& 
\sum_{h'\in\mathcal{C}'}L_i(h')\sum_{h\in{h'}^{-1}\mathcal{C}h'}L_i(h)=\\
\sum_{h'\in\mathcal{C}'}L_i(h')\sum_{h\in\mathcal{C}}L_i(h)&=& 
L_i(\mathcal{C}')L_i(\mathcal{C})
\end{eqnarray}
(recall that \(g\mathcal{C}g^{-1}=\mathcal{C}\) for any \(g\in G\) and any conjugacy
class \(\mathcal{C}\subset G\)). 

\section{Peierls-like arguments}

%\bibliographystyle{unsrt}
%\bibliography{dualities}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Hopf algebras}

\subsection{Algebras}

There are several equivalent definitions of an algebra that vary in suitability
according to the context. One possibility is to think of an algebra as 
a vector space where vectors can be multiplied. So let \(A\) be a vector space 
over the a field \(k\). A multiplication, or product, in \(A\) is a map
\(\mu:A\times A\rightarrow A\) that is associative
\begin{equation}
\mu(a,\mu(b,c))=\mu(\mu(a,b),c)
\end{equation}
and bilinear
\begin{equation}
\mu(\beta a+\gamma b,c)=\beta\mu(a,c)+\gamma\mu(b,c),\ \ \ \ \ \ 
\mu(a,\alpha b+\beta c)=\alpha\mu(a,b)+\beta\mu(a,c).
\end{equation}
The algebra A is called {\it unital} if there is an element \(e\in A\) such
that 
\begin{equation}
\mu(e,b)=\mu(b,e)=b
\end{equation} 
If it exists, the unit is unique and defines a unique embedding \(\eta:k\rightarrow A\)
of the ground field \(k\) in \(A\), as follows
\begin{equation}
\forall z \in k,\ \ \ \ \eta(z)=ze
\end{equation} 
\(\eta\) is called the identity map. Notice that if we are given \(\eta\) rather than
\(e\), then we can recover the latter as \(e=\eta(1)\).


For this definition alone, let \(k=\mathbb{C}\). 
\(A\) is a star algebra if there is an operation \({}^*:A\rightarrow A\)
such that
\begin{equation}
(\lambda a+\gamma b)^*=\bar{\lambda}a^*+\bar{\gamma} b^*,\ \ \ \ \ \ 
(ab)*=a^*b^*
\end{equation}

An important finite dimensional algebra is the \(*-\)algebra of rectangular
matrices \(M_{n,m}(\mathbb{C})\), where Hermitian conjugation is the \({*-}\)operation.
If (and only if) \(n=m\), the algebra \(M_{n,n}(\mathbb{C})\equiv M_{n}(\mathbb{C})\) 
of square matrices is unital.

Purely from the point of view of linear algebra, \(\mu\) is a bilinear map, and
so we can use it to define a {\it linear} map on \(A\otimes A\). We denote it by the
same letter \(\mu:A\otimes A\rightarrow A\), and is defined on elementary tensors as
\begin{equation}
\mu(a\otimes b)=\mu(a,b)
\end{equation}
We call this map also a multiplication. 

\subsection{Coalgebras}

To motivate the introduction of coalgebras, let us notice that any finite dimensional
algebra defines automatically a corresponding coalgebras. In a very definite sense,
a coalgebra is the dual representation of an algebra. Suppose \(A\) is an algebra as before. 
Can we use the multiplication on \(A\) to induce a
multiplication on \(A^*\)? In general, no. What we can do if \(A\) is finite dimensional
is use the algebra structure
on \(A\) to endow \(A^*\) with a coalgebra structure.  Thus in finite dimensions, any algebra
problem can be mapped to a coalgebra problem and {\it viceversa}. To see how this works, let us 
recall:

{\it Duality in linear algebra.}  Let \(A\) be a vector space over the field \(k\).
The dual space is the vector space
\begin{equation}
A^*=\{ w:A\rightarrow k\ | w \mbox{linear map}\} 
\end{equation}
Consider next any linear map 
\begin{equation}
T:A\rightarrow B
\end{equation}
\(T\) defines a unique linear map \(T^*:B^*\rightarrow A^*\) (the mapping
\(T\mapsto T^*\) is contravariant). For every \(v\in B^*, a\in A\),
\begin{equation}
(T^*v)(a)=v(Ta)
\end{equation}
T*, a linear map on functionals, is the map dual to T.

As we saw already, a (unital) algebra can be specified by three pieces of data \((A,\mu,\eta)\),
a \(k\)-vector space, a {\it linear} multiplication map \(\mu:A\otimes A\rightarrow A\),
and a linear identity map \(\eta:k\rightarrow A\) that satisfy a few compatibility conditions.
in finite dimensions, the dual set of data \((A^*,\mu^*,\eta^*)\) afford an equivalent, yet
rather different looking, representation of an algebra. The map
\begin{equation} 
\mu^* (A\otimes A)^*\cong A^*\otimes A^*\rightarrow A^*
\end{equation}
defines a comultiplication on \(A^*\), and 
\begin{equation}
\eta^*:A^*\rightarrow k^*\cong k
\end{equation}
a counit. So, in finite dimensions, if \((A,\mu,\eta)\) is an algebra
\((A^*,\mu^*,\eta^*)\) is a coalgebra.

{\it A word on \(\cong\).} The symbol \(\cong\) denotes canonical (natural) mappings
that are bijective and hence can be used to identify the (structured) sets that it puts
in correspondence. Take for example \(k^*\cong k\). \(k^*\) is the space of linea functionals
on \(k\), where \(k\) is thought of as a vector space over itself. Let \(w\in k^*\). Then
\begin{equation}
\forall z\in k,\ \ \ \ w(z)=w(z1)=zw(1).
\end{equation} 
The mapping \(w\mapsto w(1)\) affords a canonical identification of \(k^*\) with \(k\).
What is the meaning of \((A\otimes A)^*\cong A^*\otimes A^*\) ? Let \(v, w \in A^*\) and
\(a,b\in A\). Then the pairing
\begin{equation}
(v\otimes w)(a\otimes b)=v(a)w(b)
\end{equation}
maps \(A^*\otimes A^*\) into \((A\otimes A)^*\). If \(A\) is finite-dimensional,
the mapping is bijective, i.e. an identification.

{\it Example: Coalgebra of the algebra of quaternions.}
The four-dimensional \(k-\)vector space \(k^4\) can be endowed with the quaternion
multiplication. Let \(e_0,e_1,e_2,e_3\) denote its canonical basis. Then
\(\mu:k^4\otimes k^4\rightarrow k^4\) is defined by
\begin{eqnarray}
\mu(e_0\otimes e_\nu)=\mu(e_\nu\otimes e_0)=e_\nu,\ \ \ \ \nu=0,1,2,3,4\\
\mu(e_i\otimes e_j)=\epsilon_{ijk}e_k=-\mu(e_j\otimes e_i),\ \ \ \ i=1,2,3
\end{eqnarray}
The identity map \(\eta:k\rightarrow k^4\) is defined as
\(\forall z\in k, \eta(z)=ke_0\). We denote this \(k\)-quaternion algebra
as \(\mathbb(H)(k)\).

Let \(\delta^\mu\in (k^4)^*\) be the dual canonical basis 
\(\delta^\mu(e_\nu)=\delta^{\mu}_\nu\). We compute the action of the 
comultiplication \(\mu^*\) on this basis. This amounts to solving a set of equations
(\(i=1,2,3\), \(\nu=0,1,2,3\)):
\begin{eqnarray}
\mu^*(\delta^i)(e_0\otimes e_\nu)&=&\delta^i(\mu(e_0\otimes e_\nu))=\delta^i(e_\nu)=
\delta^i_\nu=\mu^*(\delta^i)(e_\nu\otimes e_0),\\
\mu^*(\delta^i)(e_j\otimes e_k)&=&\delta^i(\epsilon_{jkl}e_l)=\epsilon_{jkl}\delta^i_l
=-\mu^*(\delta_1)(e_k\otimes e_j)
\end{eqnarray}
The solution for \(\mu^*(\delta^i)\) reads
\begin{equation}
\mu^*(\delta^i)=\epsilon^{ijk}\delta^j\otimes\delta^k+\delta^0\otimes\delta^i+
\delta^i\otimes\delta^0,\ \ \ \ i=1,2,3. 
\end{equation}
Next we compute \(\mu^*(\delta^0)\). By definition
\begin{eqnarray}
\mu^*(\delta^0)(e_i\otimes e_j)&=&\delta^0(\epsilon^{ijk}e_k)=0,\\
\mu^*(\delta^0)(e_0\otimes e_\nu)&=&\delta^0_\nu=\mu^*(\delta^0)(e_\nu\otimes e_0).
\end{eqnarray}
The solution \(\mu^*(\delta^i)\) reads 
\begin{equation}
\mu^*(\delta^0)=\delta^0\otimes\delta^0 .
\end{equation}
This completes the specification of the comultiplication in \((k^4)^*\) inherited
from the quaternion multiplication on \(k^4\). Next we need to compute the counit
\(\eta^*:(k^4)^*\rightarrow k^*\cong k\) dual to the unit \(\eta(z)=ze_0\). 
By definition, 
\begin{equation}
\eta^*(\delta^\nu)(z)=\delta^\nu(\eta(z))=z\delta^\nu(e_0)=z\delta^\nu_0,
\end{equation}
so that 
\begin{equation}
\eta^*(\delta^\nu)=\delta^\nu_0
\end{equation}
This completes the description of the coalgebra \((\mathbb(M)(k)^*,\mu^*,\eta^*)\) associated
to the algebra \((\mathbb(M)(k),\mu,\eta)\) of \(k\)-quaternions

The mapping \(\mu^*,\eta^*\) satisfy strong constraints inherited from their construction
starting with an algebra. \(\mu^*\) is coassociative, meaning that
\begin{equation}
(\mathds{1}\otimes \mu^*)\circ \mu^*=(\mu^*\otimes \mathds{1})\circ\mu^*,
\end{equation}
where the circle denotes composition of maps; while \(\eta^*\) is a coidentity.
To explain this last observation we need to introduce two more identifications
\(\cong\)
\begin{equation}
k\otimes A\cong A,\ \ \ \ z\otimes a\mapsto za,\ \ \ \ \ \ 
A\otimes k \cong A,\ \ \ \ a\otimes z\mapsto za. 
\end{equation}
Now consider the maps \((\mathds{1}\otimes \eta^*)\circ \mu^*:A\rightarrow
A\otimes k\), \((\eta^*\otimes\mathds{1})\circ \mu^*:A\rightarrow k\otimes A\).
We will use the identifications just introduced to think of them as maps 
``from \(A\) to \(A\)". Then we have that
\begin{equation}
(\eta^*\otimes\mathds{1}\circ \mu^*=\mathds{1}=(\mathds{1}\otimes \eta^*)\circ \mu^*.
\end{equation}

Let's show how this works for the coalgebra \((\mathbb(M)(k)^*,\mu^*,\eta^*)\):
\begin{eqnarray}
(\mathds{1}\otimes \eta^*)\circ \mu^*(\delta^0)&=&\delta^0\eta^*(\delta^0)=\delta^0,\\
(\mathds{1}\otimes \eta^*)\circ \mu^*(\delta^i)&=&\epsilon^{ijk}\delta^j\eta^*(\delta^k)
+\delta^0\eta^*(\delta^i)+\delta^i\eta^*(\delta^0)=0+0+\delta^i.
\end{eqnarray}

The above should serve to motivate the {\it definition of a coalgebra}:
A \(k\)-vector space \(C\) together with linear maps \(\Delta:A\otimes A\rightarrow
A\) and \(\epsilon:A\rightarrow k\) is a coalgebra if the comultiplication
\(\Delta\) is coassociative and \(\epsilon\) is a counit for \(\Delta\). 
It is clear that if \(C\) is  finite dimensional, the dual 
\(C^*,\Delta^*, \epsilon^*\) is an algebra.

\section{Bialgebras}
A k-vector space \(A\) that is both an algebra and a coalgebra is called a bialgebra,
and denoted \((A,\mu,\eta,\Delta,\epsilon)\) provided some compatibility conditions 
are met. The point to notice is that if \((A,\mu,\eta\) is an algebra, so is
\((A\otimes A, (\mu\otimes\mu)\circ(\mathds{1}\otimes\tau\otimes\mathds{1},
\eta\otimes\eta)\), where the flip map \(\tau:A\otimes A\rightarrow A\otimes A\)
is defined as
\begin{equation}
\tau(a\otimes b)=b\otimes a
\end{equation}
Since the comultiplication \(\Delta\) maps \(A\) to \(A\otimes A\), the 
question arises wether \(\Delta\) is an homomorphism of algebra. If the answer
is in the affirmative, and if \(\epsilon:A\rightarrow k\) is also an homomorphism
of algebras (\(k\) is a \(k\)-algebra ), then we say that \((A,\mu,\eta,\Delta,\epsilon)\)
is a bialgebra.

{\color{red} I guess this means that \((A,\mu,\eta)\) and \((A^*,\Delta^*, \epsilon^*)\)
are homomorphic? }

Bialgebras are interesting because they permit to define an alternative multiplication
of linear maps. Let \(T_1, T_2:A\rightarrow A\) be linear maps. The standard way to 
multiply them is to compose them, \(T_1T_2\equiv T_1\circ T_2\). 
But if \(A\) is a bialgebra, we can also define the convolution multiplication
of \(T_1, T_2\) as
\begin{equation}
T_1\star T_2\equiv \mu\circ (T_1\otimes T_2)\circ \Delta .
\end{equation}
The convolution affords an alternative associative multiplication of linear maps, with identity
\(\eta\circ\epsilon\)
\begin{equation}
T\star(\eta\circ\epsilon)=\mu\circ(T\otimes(\eta\circ\epsilon))\circ\Delta= T=
(\eta\circ\epsilon)\star T
\end{equation}

In summary, if \(A\) is a bialgebra, the space \(Hom(A)\) of linear transformations of
\(A\) admits two different unital algebra structures, one with composition and the other with
convolution for multiplication that we denote respectively as
\((Hom(A),\circ,\mathds{1})\), and \((Hom(A),\star, \eta\circ\epsilon)\). 
A simple attempt to compare the two leads to the notion of Hopf algebra.

\subsection{Hopf algebras}
Given a bialgebra \((A,\mu,\eta,\Delta,\epsilon)\), what is the relations between the
algebras of operators \((Hom(A),\circ,\mathds{1})\) and \((Hom(A),\star, \eta\circ\epsilon)\)?
The first basic question to answer is the following: suppose \(T\) is invertible in
\((Hom(A),\circ,\mathds{1})\). Is it also invertible in 
\((Hom(A),\star, \eta\circ\epsilon)\)? We can reduce this problem to that of deciding whether
\(\mathds{1}\) is invertible in \((Hom(A),\star, \eta\circ\epsilon)\). Hence we are looking
for an operator \(S\) such that 
\begin{equation}
S\star\mathds{1}=\mathds{1}\star S=\eta\circ\epsilon.
\end{equation}
{\it If \(S\) exists (and it does not always exist), we call it the antipode of the 
the bialgebra \(A\), and \((A,\mu,\eta,\Delta,\epsilon, S)\) is a Hopf algebra}.

{\it Example: Group bialgebra and Hopf algebra.} Let \(G\) be a finite group,
and \(A(G)\) the free \(k\)-vector space generated by \(G\). The following operations
turn \(A(G)\) into a bialgebra:
\begin{eqnarray}
\mu(g\otimes h)&=&gh,\ \ \ \ \ \ \eta(z)=z e,\\
\Delta(g)&=&g\otimes g,\ \ \ \ \ \ \epsilon(g)=1.
\end{eqnarray}
In physical contexts it may be convenient to denote the basis vector \(g\) by
\(|g\rangle\). Let's show that \(\Delta\) and \(\epsilon\) are algebra morphisms.
\begin{eqnarray}
\Delta\mu(g\otimes h)=\Delta(gh)=gh\otimes gh= (\mu\otimes \mu)(\mathds{1}\otimes\sigma
\otimes\mathds{1})(\Delta(g)\otimes\Delta(h)),\\
\epsilon(gh)=1=1.1=\epsilon(g)\epsilon(h).
\end{eqnarray}
Recall that \((\mu\otimes \mu)(\mathds{1}\otimes\sigma
\otimes\mathds{1})\) is the canonical multiplication on \(A\otimes A\) inherited
from A,
\begin{equation}
(\mu\otimes \mu)(\mathds{1}\otimes\sigma
\otimes\mathds{1})((a\otimes b)\otimes (a'\otimes b'))=
(\mu\otimes \mu)(a\otimes a'\otimes b\otimes b')=
\mu(a\otimes a')\otimes \mu(b\otimes b') = aa'\otimes bb'
\end{equation}

Let \(T_1,T_2\in Hom(A(G))\). Then their convolution product reads 
\begin{equation}
T_1\star T_2)(g)=\mu(T_1\otimes T_2)\Delta(g)=\mu(T_1(g)\otimes T_2(g))
\end{equation}
In matrix form, \(T_i(g)=\sum_m m(T_i)^m_{\ \ g},\ i=1,2\), and
\begin{equation}
\sum_m m(T_1\star T_2)^m_{\ \ g}=\sum_{n,n'}nn'(T_1)^n_{\ \ g}(T_2)^{n'}_{\ \ g}.
\end{equation}
Hence
\begin{equation}
(T_1\star T_2)^m_{\ g}=\sum_{nn'=m}(T_1)^n_{\ \ g}(T_2)^{n'}_{\ \ g}=
\sum_n(T_1)^n_{\ g}(T_2)^{n^{-1}m}_{\ \ \ g}.
\end{equation}

Now we can compute the antipode from the equation
\begin{equation}
(S\star\mathds{1})(g)=\mu(S(g)\otimes g)=\eta\epsilon(g)=e=(\mathds{1}\star S)(g).
\end{equation}
It follows that
\begin{equation}
S(g)=g^{-1}
\end{equation}

This completes the specification of the group Hopf algebra. Notice that 
\(\mu\) is not commutative in general (\(\mu\sigma\neq \mu\)), but 
\(\Delta\) is cocommutative always (\(\sigma\Delta=\Delta\)).
